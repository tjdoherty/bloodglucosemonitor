using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    /*
     * Program: FormPatient 
     * Derived: Windows Form object
     * Purpose: Allow the user to read, add, update, and delete rows
     *          in the Patient table in the BloodGlucoseMonitor database
     * Author: Yunsheng Ma, Tim Doherty
     * Course: SDEV240 00c Software Development using C#
     * Assignment: Final Group Project (Green Group)
     * Date Written: 11/27/2016
     * Modifications:   11/27/2016  - Created Version 1.0
     *                  
     * 
     * 
     * 
     * 
     * 
     */
    public partial class FormPatient : Form
    {
            // Connection object for database connection
        protected SqlConnection dbConnection;

            // Data adapter objects for add, update, and delete operations
        protected SqlDataAdapter dbAdapter;

            // Data reader object for read operations
        protected SqlDataReader dbReader;

           // Parameter object used for update, delete, and single row reads.
        protected SqlParameter dbParameter;


        public FormPatient()
        {
            InitializeComponent();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmPatient_Load(object sender, EventArgs e)
        {

        }
            // build connection string and connection to database
            // returns true if connection succeeds, false if it fails
        private void ConnectToDB(bool ret)
        {
        }

        private void DeletePatient(int patientID)
        {

        }

        private void GetPatient(int patientID)
        {

        }

        private void AddNewPatient()
        {


        }


        private void UpdatePatient(int patientID)
        {


        }
        private void FillForm()
        {




        }

        private void txtFirtsName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
