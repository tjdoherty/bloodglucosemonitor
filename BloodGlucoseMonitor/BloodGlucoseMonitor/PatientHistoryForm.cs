﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlucoseGUI
{
    public partial class PatientHistory : Form
    {
        public PatientHistory()
        {
            InitializeComponent();
        }

        private void AddressLabel_Click(object sender, EventArgs e)
        {

        }

        private void ResultButton_Click(object sender, EventArgs e)
        {

        }

        private void GetHistory()
        {
            PatientHistory history = new PatientHistory
            {
                PatientID = Int32.Parse(PatientIdTextBox.Text),
                DateEntered = PatientCalender.SelectionRange.Start
            };
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
