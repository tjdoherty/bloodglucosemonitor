﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlucoseGUI
{
    public partial class PatientDosage : Form
    {
        public PatientDosage()
        {
            InitializeComponent();
        }

        private void GetDosageButton_Click(object sender, EventArgs e)
        {
            DosageAmountLabel.Text = 0.ToString();//MAKE THIS THE RESULT
            GetDosage();
        }

        private void GetDosage()
        {
            PatientDosage dosage = new PatientDosage
            {
                BaseGlucoseLevel = Double.Parse(PatientGlucoseLevelTextBox.Text),
                PatientID = Int32.Parse(PatientIdTextBox.Text),
                BeginDate = BeginDateCalender.SelectionRange.Start,
                EndDate = EndDateCalender.SelectionRange.Start
            };
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
