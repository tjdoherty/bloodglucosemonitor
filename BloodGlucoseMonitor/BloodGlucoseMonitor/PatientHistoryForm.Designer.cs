﻿namespace GlucoseGUI
{
    partial class PatientHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DosageScheduleLabel = new System.Windows.Forms.Label();
            this.ResultLabel = new System.Windows.Forms.Label();
            this.PatientHistoryLabel = new System.Windows.Forms.Label();
            this.OkButton = new System.Windows.Forms.Button();
            this.PatientIdTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PatientCalender = new System.Windows.Forms.MonthCalendar();
            this.ResultButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DosageScheduleLabel
            // 
            this.DosageScheduleLabel.AutoSize = true;
            this.DosageScheduleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DosageScheduleLabel.Location = new System.Drawing.Point(250, 9);
            this.DosageScheduleLabel.Name = "DosageScheduleLabel";
            this.DosageScheduleLabel.Size = new System.Drawing.Size(197, 25);
            this.DosageScheduleLabel.TabIndex = 2;
            this.DosageScheduleLabel.Text = "Dosage Schedule";
            // 
            // ResultLabel
            // 
            this.ResultLabel.AutoSize = true;
            this.ResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResultLabel.Location = new System.Drawing.Point(12, 347);
            this.ResultLabel.Name = "ResultLabel";
            this.ResultLabel.Size = new System.Drawing.Size(79, 25);
            this.ResultLabel.TabIndex = 3;
            this.ResultLabel.Text = "Result";
            // 
            // PatientHistoryLabel
            // 
            this.PatientHistoryLabel.AutoSize = true;
            this.PatientHistoryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PatientHistoryLabel.Location = new System.Drawing.Point(12, 9);
            this.PatientHistoryLabel.Name = "PatientHistoryLabel";
            this.PatientHistoryLabel.Size = new System.Drawing.Size(119, 18);
            this.PatientHistoryLabel.TabIndex = 4;
            this.PatientHistoryLabel.Text = "Patient History";
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(372, 351);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 8;
            this.OkButton.Text = "Ok";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // PatientIdTextBox
            // 
            this.PatientIdTextBox.Location = new System.Drawing.Point(96, 62);
            this.PatientIdTextBox.Name = "PatientIdTextBox";
            this.PatientIdTextBox.Size = new System.Drawing.Size(24, 20);
            this.PatientIdTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Patient Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 18);
            this.label2.TabIndex = 10;
            this.label2.Text = "Date";
            // 
            // PatientCalender
            // 
            this.PatientCalender.Location = new System.Drawing.Point(67, 116);
            this.PatientCalender.MaxSelectionCount = 1;
            this.PatientCalender.Name = "PatientCalender";
            this.PatientCalender.TabIndex = 11;
            // 
            // ResultButton
            // 
            this.ResultButton.Location = new System.Drawing.Point(17, 306);
            this.ResultButton.Name = "ResultButton";
            this.ResultButton.Size = new System.Drawing.Size(75, 23);
            this.ResultButton.TabIndex = 12;
            this.ResultButton.Text = "Get Result";
            this.ResultButton.UseVisualStyleBackColor = true;
            this.ResultButton.Click += new System.EventHandler(this.ResultButton_Click);
            // 
            // PatientHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 386);
            this.Controls.Add(this.ResultButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PatientCalender);
            this.Controls.Add(this.PatientIdTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.PatientHistoryLabel);
            this.Controls.Add(this.ResultLabel);
            this.Controls.Add(this.DosageScheduleLabel);
            this.Name = "PatientHistory";
            this.Text = "PatientHistory";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label DosageScheduleLabel;
        private System.Windows.Forms.Label ResultLabel;
        private System.Windows.Forms.Label PatientHistoryLabel;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.TextBox PatientIdTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MonthCalendar PatientCalender;
        private System.Windows.Forms.Button ResultButton;
    }
}